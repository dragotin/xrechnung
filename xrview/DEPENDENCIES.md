## Build Dependencies

XRView is built against Qt 6. Any sub version should do.

For decompression of zip files, KArchive from the KDE Frameworks 6 is 
used.

## Runtime Dependencies

XRView uses java to run the saxon XSLT processor. Any modern java processor
installed on the system should be suitable.


