## XRView

XRView is a Qt based viewer for the Desktop that renders XRechnung XML documents. It uses the official XSL stylesheets provided in [the repository](https://github.com/itplr-kosit/xrechnung-visualization) of the [Koordinierungsstelle für IT-Standards](https://www.xoev.de/).

![XRView screenshot](/xrview/screenshot1.png?raw=true&s=200 "Screenshot")

Beside the rendering, it also extracts some important data from the XML and displays them in the overview pane.

### Configuration

XRView will download the required resources on first start if the user allows to do so.

That will download what is needed, unpack, and create a config file in `$HOME/.config/xrview/xrview.conf`.

The config file looks like:

```bash
[saxon]
jar=/real/path/saxon-he-11.4.jar
xslHtml=/real/path/xrechnung-visualization/src/xsl/xrechnung-html.xsl
xslUbl=/real/path/xrechnung-visualization/src/xsl/ubl-invoice-xr.xsl
```

The files are downloaded from this sources:

1. Saxon:  https://raw.githubusercontent.com/Saxonica/Saxon-HE/main/11/Java/SaxonHE11-4J.zip
2. xrechnung-visualization XSLT sheets: https://projekte.kosit.org/xrechnung/xrechnung-visualization/-/archive/master/xrechnung-visualization-master.zip

### Usage

Start the viewer from the command line and pass one or more XML files in XRechnung format on the command line.

Example:
```bash
xrview /real/path/3321-342.xml
```

There is also a file menu that allows opening XRechnung files in a file selector.

### Build

XRView is written in C++ using the Qt toolkit and cmake. Build as usual.

