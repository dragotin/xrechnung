<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/main.cpp" line="55"/>
        <source>File</source>
        <translation type="unfinished">Datei</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="43"/>
        <location filename="../src/mainwindow.cpp" line="90"/>
        <source>XRechnung Viewer</source>
        <translation type="unfinished">XRechnung Anzeige</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="91"/>
        <source>The needed resources to display the document could not be found.

Please remove the configuration file ~/.config/xrview/xrview.conf and restart the viewerto foster the automatic download again.</source>
        <translation type="unfinished">Eine Resource, die zum Anzeigen des Dokumentes nötig ist, kann nicht gefunden werden.

Bitte löschen Sie die Konfigurationsdatei ~/.config/xrview/xrview.conf und starten Sie das Programm neu, um den automatischen Download auszulösen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="101"/>
        <source>Resource Download</source>
        <translation type="unfinished">Resourcen Herunterladen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="102"/>
        <source>Some of the required resource files could not be found.

Should they be downloaded from Github?</source>
        <translation type="unfinished">Einige der notwendigen Resourcen konnten nicht gefunden werden.

Sollen sie aus dem Internet heruntergeladen werden?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="104"/>
        <source>Start download</source>
        <translation type="unfinished">Download starten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="112"/>
        <source>All resources loaded, ready to load ✔️</source>
        <translation type="unfinished">Alle Resourcen sind heruntergeladen, fertig zum Laden von Dateien</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="112"/>
        <source>Resource error, can not load XRechnung</source>
        <translation type="unfinished">Resourcenfehler, XRechnung kann nicht geladen werden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="121"/>
        <source>Resources not available, download not wanted.</source>
        <translation type="unfinished">Resourcen fehlen und Download ist nicht erwünscht.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="124"/>
        <source>All resources available, ready to load XRechnung ✔️</source>
        <translation type="unfinished">Alle Resourcen vorhanden, XRechnung kann geladen werden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <source>XRView - XRechnung Viewer</source>
        <translation type="unfinished">XRView - XRechnung anzeigen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="189"/>
        <source>&lt;b&gt;XRechnung Viewer Version %1&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;XRView is a utility to display XRechnung documents.&lt;br/&gt;&lt;br/&gt;This program is released under GPL v3&lt;br/&gt;Please contribute at https://codeberg.org/dragotin/xrechnung&lt;br/&gt;&lt;br/&gt;Author: Klaas Freitag &amp;lt;opensource@freisturz.de&amp;gt;</source>
        <translation type="unfinished">&lt;b&gt;XRechnung Anzeigen Version %1&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;XRView ist ein Programm um XRechnung Dokumente anzuzeigen.&lt;br/&gt;&lt;br/&gt;Dieses Programm wurde unter der Lizenz GPL v3 herausgegeben.&lt;/br/&gt;Bitte tragen Sie dazu unter https://codeberg.org/dragotin/xrechnung bei&lt;br/&gt;&lt;br/&gt;Autor: Klaas Freitag &amp;lt;opensource@freisturz.de&amp;gt;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="43"/>
        <source>xrechnung</source>
        <translation type="unfinished">xrechnung</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="43"/>
        <source>List of xrechnung</source>
        <translation type="unfinished">Liste von XRechnungen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="132"/>
        <source>Open XRechnung</source>
        <translation type="unfinished">XRechnung öffnen</translation>
    </message>
    <message>
        <location filename="../src/xrview.h" line="35"/>
        <source>Viewer for XRechnung documents.</source>
        <translation type="unfinished">Anzeige von XRechnung Dokumenten.</translation>
    </message>
    <message>
        <location filename="../src/xrview.h" line="36"/>
        <source>GNU General Public License Version 3</source>
        <translation type="unfinished">GNU General Public License Version3</translation>
    </message>
</context>
<context>
    <name>XRWidget</name>
    <message>
        <location filename="../src/xrwidget.cpp" line="51"/>
        <source>Leitweg-ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="52"/>
        <source>Date:</source>
        <translation type="unfinished">Datum:</translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="53"/>
        <source>Buyer:</source>
        <translation type="unfinished">Käufer:</translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="54"/>
        <source>Net:</source>
        <translation type="unfinished">Netto:</translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="55"/>
        <source>VAT:</source>
        <translation type="unfinished">MwSt:</translation>
    </message>
    <message>
        <location filename="../src/xrwidget.cpp" line="56"/>
        <source>Gross:</source>
        <translation type="unfinished">Brutto:</translation>
    </message>
</context>
<context>
    <name>XRechnung</name>
    <message>
        <location filename="../src/xrechnung.cpp" line="141"/>
        <source>Error while creating UBL</source>
        <translation type="unfinished">Fehler beim Erzeugen des UBL-Dokuments</translation>
    </message>
    <message>
        <location filename="../src/xrechnung.cpp" line="179"/>
        <source>Error while creating HTML</source>
        <translation type="unfinished">Fehler beim Erzeugen des HTML Dokuments</translation>
    </message>
</context>
</TS>
