/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2023 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "xrechnung.h"
#include "xrview.h"
#include "xrwidget.h"

#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QTimer>
#include <QSettings>
#include <QObject>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->_mainToolBox->removeItem(0);

    connect(ui->_mainToolBox, &QToolBox::currentChanged, this, &MainWindow::slotShowDocumentNo);
    setWindowTitle(tr("XRechnung Viewer"));

    // restore window geometry
    QSettings config;
    const auto geo = QByteArray::fromBase64(config.value(GeoWindow).toByteArray());
    restoreGeometry(geo);

    const QStringList ints = config.value(GeoSplitter).toString().split('/');
    if (ints.size() > 1) {
        QList<int> s {ints.at(0).toInt(), ints.at(1).toInt()};
        ui->splitter->setSizes(s);
    }

    // The controller gets a list of urls to laod the XRechnung docs from.
    // This connection cares for displaying the XRechnung.
    QObject::connect(&_xrc, &XRControl::showXRechnung, this, &MainWindow::slotShowXRechnung);

    // connect menu items
    QObject::connect(ui->actionLoad, &QAction::triggered, this, &MainWindow::slotLoadFile);
    QObject::connect(ui->actionExit, &QAction::triggered, QCoreApplication::instance(), &QCoreApplication::quit);
    QObject::connect(ui->actionAbout_XRView, &QAction::triggered, this, &MainWindow::about);

    QTimer::singleShot(0, this, &MainWindow::checkConfig);
}

MainWindow::~MainWindow()
{
    QSettings config;
    const QByteArray geo = saveGeometry().toBase64();
    config.setValue(GeoWindow, QString::fromLatin1(geo));

    const QList<int> s = ui->splitter->sizes();
    if (s.size()>1)
        config.setValue(GeoSplitter, QString("%1/%2").arg(s.at(0)).arg(s.at(1)));

    config.sync();

    delete ui;
}

void MainWindow::checkConfig()
{
    auto stateMap = _setup.check();

    if (stateMap[XRechnung::SaxonHtml] == XRVSetup::State::Error||
            stateMap[XRechnung::SaxonUbl] == XRVSetup::State::Error||
            stateMap[XRechnung::SaxonJar] == XRVSetup::State::Error) {
        QMessageBox::information(this, tr("XRechnung Viewer"),
                                 tr("The needed resources to display the document could not be found.\n\n"
                                    "Please remove the configuration file ~/.config/xrview/xrview.conf and restart the viewer"
                                    "to foster the automatic download again."));

        return;
    }

    if (stateMap[XRechnung::SaxonHtml] != XRVSetup::State::Available ||
            stateMap[XRechnung::SaxonUbl] != XRVSetup::State::Available ||
            stateMap[XRechnung::SaxonJar] != XRVSetup::State::Available) {
       auto btn = QMessageBox::question(this, tr("Resource Download"),
                                        tr("Some of the required resource files could not be found.\n\nShould they be downloaded from Github?"));
        if (btn == QMessageBox::Yes) {
            statusBar()->showMessage(tr("Start download"));
            connect(&_setup, &XRVSetup::done, [=](QMap<QString, XRVSetup::State> states) {
                bool cool{true};
                if (states[XRechnung::SaxonHtml] != XRVSetup::State::Available ||
                    states[XRechnung::SaxonUbl] != XRVSetup::State::Available ||
                        states[XRechnung::SaxonJar] != XRVSetup::State::Available) {
                    cool = false;
                }
                statusBar()->showMessage(cool ? tr("All resources loaded, ready to load ✔️") : tr("Resource error, can not load XRechnung"));
            });

            // The controller gets a list of urls to laod the XRechnung docs from.
            // This connection cares for displaying the XRechnung.
            connect(&_setup, &XRVSetup::done, this, &MainWindow::slotShowFileList);
            _setup.doDownload(stateMap);
        } else {
            qDebug() << "No download wanted.";
            statusBar()->showMessage(tr("Resources not available, download not wanted."));
        }
    } else {
        statusBar()->showMessage(tr("All resources available, ready to load XRechnung ✔️"));
        QTimer::singleShot(0, this, &MainWindow::slotShowFileList);
    }

}

void MainWindow::slotLoadFile()
{
    QStringList filesStr = QFileDialog::getOpenFileNames(this, QObject::tr("Open XRechnung"),
                                                         QDir::homePath(),
                                                         "*.xml");
    QList<QUrl> urls;
    for( const QString& f : filesStr) {
        QUrl url{QUrl::fromLocalFile(f)};
        urls.append(url);
    }
    _xrc.registerFiles(urls);
    _files.append(urls);
}

void MainWindow::slotShowFileList()
{
    int cnt = ui->_mainToolBox->count();

    if (cnt > 0) {
        ui->_mainToolBox->blockSignals(true);
        for (int i=0; i < cnt; i++) ui->_mainToolBox->removeItem(i);
        ui->_mainToolBox->blockSignals(false);
    }
    _xrc.registerFiles(_files);
}

void MainWindow::setFileList(const QList<QUrl>& fileList)
{
    _files = fileList;
}

void MainWindow::slotShowXRechnung(XRechnung *xr)
{
    if (xr == nullptr) return;

    ui->_mainToolBox->blockSignals(true);
    int indx = ui->_mainToolBox->addItem(new XRWidget(xr, this), xr->fileName());
    qDebug() << "Added index:" << indx;

    connect( xr, &XRechnung::htmlAvailable, this, [=]() {
        this->slotShowDocumentNo(indx);
    });
    ui->_mainToolBox->setCurrentIndex(indx);
    ui->_mainToolBox->blockSignals(false);
}

void MainWindow::slotShowDocumentNo(int newIndx)
{
    XRWidget *item = qobject_cast<XRWidget*>(ui->_mainToolBox->widget(newIndx));

    XRechnung *xr = item->xrechnung();

    const QString h = xr->html();
    ui->_htmlView->setHtml(h);
}

void MainWindow::about()
{
    QMessageBox::information(this, tr("XRView - XRechnung Viewer"),
                             tr("<b>XRechnung Viewer Version %1</b><br/><br/>XRView is a utility to display XRechnung documents.<br/><br/>"
                                "This program is released under GPL v3<br/>"
                                "Please contribute at https://codeberg.org/dragotin/xrechnung<br/><br/>"
                                "Author: Klaas Freitag &lt;opensource@freisturz.de&gt;").arg(XRView::Version::humanReadable()));
}
