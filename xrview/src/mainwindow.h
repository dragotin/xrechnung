/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2023 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "xrechnung.h"
#include "xrvsetup.h"
#include "xrcontrol.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void slotShowXRechnung(XRechnung *);
    void setFileList(const QList<QUrl>& fileList);

private slots:
    void slotShowDocumentNo(int);
    void slotLoadFile();
    void about();
    void slotShowFileList();
    void checkConfig();

private:

    const QString GeoWindow{"geometry/mainwindow"};
    const QString GeoSplitter{"geometry/mainsplitter"};

    Ui::MainWindow *ui;
    XRVSetup _setup;
    XRControl _xrc;
    QList<QUrl> _files;
};
#endif // MAINWINDOW_H
