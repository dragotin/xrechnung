/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2024 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VERSION_H
#define VERSION_H

#include <QString>
#include <QObject>

namespace XRView {
namespace Version {
// Static content. Maintain values here.
inline QString humanReadable() { return QStringLiteral("1.0"); }
}

namespace Info {
inline QString authors() { return QStringLiteral("Klaas Freitag <opensource@freisturz.de>"); }

inline QString description() { return QObject::tr("Viewer for XRechnung documents."); }
inline QString versions() { return QObject::tr("GNU General Public License Version 3"); }
}
}

#endif // VERSION_H
