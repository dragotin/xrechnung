/*
 * XRechnung Viewer Suite
 *
 * Copyright (C) 2024 Klaas Freitag <opensource@freisturz.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QSettings>
#include <QFileInfo>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QStandardPaths>
#include <QTemporaryFile>

#include <kzip.h>

#include "xrview.h"
#include "xrvsetup.h"
#include "xrechnung.h"

XRVSetup::XRVSetup(QObject *parent)
    : QObject{parent}
{
    QObject::connect(&_nam, &QNetworkAccessManager::finished, this, &XRVSetup::downloadFinished);

    QString path{QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)};

    if (path.isEmpty()) {
        path = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    }
    _saxonDir.setPath(path);

    // check if the local dir is existing
    if (!_saxonDir.exists()) {
        _saxonDir.mkpath(path);
    }

    qDebug() << "Writeable location is set to" << path;
}

/*
 * checks if the config file has a path to the specific file and if so,
 * if the file really exists and is readable
 */
bool XRVSetup::checkFile(const QString& fileId)
{
    bool re{true};

    if (!_config.contains(fileId)) {
        re = false;
    }

    if (re) {
        const QString file = _config.value(fileId).toString();
        QFileInfo fi(file);
        if (! (fi.exists() && fi.isReadable())) {
            re = false;
            _stateMap[fileId] = State::NotExist;
        }
    }
    if (re)
        _stateMap[fileId] = State::Available;

    return re;
}

void XRVSetup::tryDownload(const QString& fileId) {
    const QMap<QString, QByteArray> urls {
        {XRechnung::SaxonJar, "https://raw.githubusercontent.com/Saxonica/Saxon-HE/main/11/Java/SaxonHE11-4J.zip"},
        {XRechnung::SaxonXsl, "https://projekte.kosit.org/xrechnung/xrechnung-visualization/-/archive/master/xrechnung-visualization-master.zip"}
    };

    if (fileId == XRechnung::SaxonUbl) {
        // return, already handled by the html case.
        return;
    }

    QUrl url = QUrl::fromEncoded(urls[fileId]);
    qDebug() << "Attempt to download" << url;
    QNetworkRequest request(url);
    const QByteArray ua{ QString("XRechnung Viewer %1").arg(XRView::Version::humanReadable()).toUtf8() };
    request.setRawHeader("User-Agent", ua);

    QNetworkReply *reply = _nam.get(request);

    if (reply) {
        reply->setProperty(DownloadId, fileId);
        connect(reply, &QNetworkReply::downloadProgress, this, &XRVSetup::downloadProgress);
        connect(reply, &QNetworkReply::errorOccurred, [=](QNetworkReply::NetworkError code) {
            qDebug() << " * error" << code;
        });
        _stateMap[fileId] = State::Download;
    }
}

void XRVSetup::downloadProgress(qint64 recieved, qint64 total) {
    qDebug() << " * download:" << recieved << total;
}

QFileInfo XRVSetup::extractZip(const QString& fileId, const QString& zipFile)
{
    QFileInfo fi;

    KZip archive(zipFile);

    // Open the archive
    if (!archive.open(QIODevice::ReadOnly)) {
        qWarning() << "Cannot open " << zipFile;
        return fi;
    }

    // Take the root folder from the archive and create a KArchiveDirectory object.
    // KArchiveDirectory represents a directory in a KArchive.
    const KArchiveDirectory *root = archive.directory();

    // We can extract all contents from a KArchiveDirectory to a destination.
    // recursive true will also extract subdirectories.
    if (!root->copyTo(_saxonDir.absolutePath(), true)) {
        qDebug() << "Extraction of the zip" << zipFile << "failed.";
        return fi;
    }

    archive.close();

    if (fileId == XRechnung::SaxonJar) {
        // extract the jar file
        fi.setFile(_saxonDir.absolutePath(), "saxon-he-11.4.jar");
    } else if (fileId == XRechnung::SaxonXsl) {
        // extracted the entire visualization zipfile
        fi.setFile(_saxonDir.absolutePath(), "xrechnung-visualization-master/src/xsl/xrechnung-html.xsl");
    }
    return fi;
}

void XRVSetup::downloadFinished(QNetworkReply *reply) {
    const QMap<QString, QString> downloadFiles {
        {XRechnung::SaxonJar, "SaxonHE11-4J.zip"},
        {XRechnung::SaxonXsl, "xrechnung-visualization-master.zip"}
    };

    const QString fileId = reply->property(DownloadId).toString();
    qDebug() << "Download of" << fileId << "finished.";
    Q_ASSERT(!fileId.isEmpty());

    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Download error:" << reply->errorString();
        _stateMap[fileId] = State::Error;
        return;
    }

    // save the zip to a temp file
    QTemporaryFile localFile;

    if (!localFile.open()) {
        qDebug() << "Unable to open temporary file to write";
        return;
    }
    const QByteArray sdata = reply->readAll();
    localFile.write(sdata);
    localFile.close();

    if (sdata.size()>0) {
        QFileInfo fiRes = extractZip(fileId, localFile.fileName());

        qDebug() << "Received data for" <<fiRes.absoluteFilePath();
        _stateMap[fileId] = State::Available;
        if (fileId == XRechnung::SaxonXsl) {
            // Assume the download and unzip worked, and the other xsl is here as well.
            _config.setValue(XRechnung::SaxonHtml, fiRes.absoluteFilePath());
            fiRes.setFile(fiRes.path(), "ubl-invoice-xr.xsl");
            _config.setValue(XRechnung::SaxonUbl, fiRes.absoluteFilePath());
            _stateMap[XRechnung::SaxonUbl] = State::Available;
            _stateMap[XRechnung::SaxonHtml] = State::Available;
        } else {
            _config.setValue(fileId, fiRes.absoluteFilePath());
        }
        _config.sync();
    } else {
        _stateMap[fileId] = State::Error;
    }
    validateState();
}

bool XRVSetup::finalState(const QString& fileId)
{
    State s = _stateMap[fileId];
    return (s == State::Available || s == State::Error);
}

bool XRVSetup::validateState()
{
    bool re{false};

    if (finalState(XRechnung::SaxonHtml) &&
            finalState(XRechnung::SaxonJar) &&
            finalState(XRechnung::SaxonUbl)) {
        re = true;
        Q_EMIT done(_stateMap);
    }
    return re;
}

void XRVSetup::doDownload(QMap<QString, State> state)
{
    _stateMap = state;
    if (state[XRechnung::SaxonJar] != State::Available) {
        tryDownload(XRechnung::SaxonJar);
    }
    if (state[XRechnung::SaxonHtml] != State::Available) {
        tryDownload(XRechnung::SaxonXsl);
    }
}

QMap<QString, XRVSetup::State> XRVSetup::check()
{
    qDebug() << "Reading config file" << _config.fileName();
    _stateMap.clear();
    _stateMap[XRechnung::SaxonJar] = State::Undeclared;
    _stateMap[XRechnung::SaxonUbl] = State::Undeclared;
    _stateMap[XRechnung::SaxonHtml] = State::Undeclared;

    checkFile(XRechnung::SaxonJar);
    checkFile(XRechnung::SaxonUbl);
    checkFile(XRechnung::SaxonHtml);
    return _stateMap;
}
